package tor

import (
	"fmt"
	"os"
	"testing"
)

func TestTorManager(t *testing.T) {

	os.Remove("/tmp/torrc")
	file, _ := os.Create("/tmp/torrc")
	fmt.Fprintf(file, "SOCKSPort %d\nControlPort %d\nDataDirectory /tmp/tor\n", 10050, 10051)
	file.Close()
	tm, err := NewTorManager(10050, 10051, "/tmp/torrc")
	if err != nil {
		t.Errorf("creating a new tor manager failed: %v", err)
	} else {

		tm2, err := NewTorManager(10050, 10051, "/tmp/torrc")
		if err != nil {
			t.Errorf("creating a new tor manager failed: %v", err)
		}
		tm2.Shutdown() // should not noop
	}
	tm.Shutdown()
}
